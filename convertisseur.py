from pathlib import Path
from time import perf_counter

import minicli
import pypandoc

try:
    print(f"Pandoc version: {pypandoc.get_pandoc_version()}")
except OSError:
    pypandoc.download_pandoc()
    print(f"Pandoc version: {pypandoc.get_pandoc_version()}")


HERE = Path(".").resolve()


@minicli.cli
def pdf():
    css_path = HERE / "style.css"
    source = HERE / "source.md"
    output = HERE / "output.pdf"
    pypandoc.convert_file(
        str(source),
        "pdf",
        extra_args=[
            "--standalone",
            "--css",
            str(css_path),
            "--pdf-engine",
            "weasyprint",
            "--to",  # Required for weasyprint.
            "html",
            "--quiet",
        ],
        outputfile=str(output),
    )


@minicli.wrap
def perf_wrapper():
    start = perf_counter()
    yield
    elapsed = perf_counter() - start
    print(f"Done in {elapsed:.5f} seconds.")


if __name__ == "__main__":
    minicli.run()
